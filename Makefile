#------------------------------------- Makefile ----------------------------------

fclean:
	sudo rm -rf ~/data/mariadb
	sudo rm -rf ~/data/wordpress
	if [ "`docker volume ls | wc -l`" != "1" ]; then\
    	docker volume rm -f $$(docker volume ls -q);\
	fi
	if [ "`docker image ls | wc -l`" != "1" ]; then\
		docker image rm -f $$(docker image ls -q);\
	fi
up:
	if [ ! -d "~/data" ]; then mkdir -p /home/anramire/data; fi
	mkdir -p ~/data/mariadb
	mkdir -p ~/data/wordpress
	docker-compose -f ./srcs/docker-compose.yml up --build --force-recreate 

down:
	docker-compose -f ./srcs/docker-compose.yml down

