#!/bin/bash

set -e

service mariadb start

echo "CREATE USER IF NOT EXISTS '$MANAGER_USER'@'%' IDENTIFIED BY '$MANAGER_PASSWORD';" | mysql -u root

echo "GRANT ALL PRIVILEGES ON *.* TO '$MANAGER_USER'@'%' IDENTIFIED BY '$MANAGER_PASSWORD';" | mysql -u root

if [[ -z "${DEPLOY_ENV}" ]]; then
	echo "CREATE USER IF NOT EXISTS 'default'@'%' IDENTIFIED BY 'default';"
else
	echo "CREATE USER IF NOT EXISTS '$DB_USER'@'%' IDENTIFIED BY '$DB_PASSWORD;'"
fi

if [[ -z "${DEPLOY_ENV}" ]]; then
	echo "CREATE DATABASE IF NOT EXISTS default; GRANT ALL ON default.* TO 'default'@'%' IDENTIFIED BY 'default'; FLUSH PRIVILEGES;" | mysql -u root

else 
	echo "CREATE DATABASE IF NOT EXISTS $DATABASE; GRANT ALL ON $DATABASE.* TO '$DB_USER'@'%' IDENTIFIED BY '$DB_PASSWORD'; FLUSH PRIVILEGES;" | mysql -u root
fi
service mariadb stop
echo "Executing mysqld..."
mysqld
echo "Finishing container!!!"
