#!/bin/bash

set -e

if ! wp core is-installed --allow-root; then
	wp core install --url=anramire.42.fr --title=Example_web --admin_user=$WP_ADMIN_USER --admin_password=$WP_ADMIN_PASS --admin_email=$WP_ADMIN_GMAIL

	wp user create $WP_USER $WP_GMAIL --user_pass=$WP_PASS 2> /dev/null
fi

echo "Executing php-fpm on port 9000..."
php-fpm81 -F -R 
echo "Ending php-fpm!"
